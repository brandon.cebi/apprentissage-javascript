const inputText = document.querySelector('#input');
const themeMode = document.querySelectorAll('.theme')
const darkMode = document.querySelector('#dark');
const salmonMode = document.querySelector('#salmon');
const whiteMode = document.querySelector('#basic');

inputText.addEventListener('input', (e) => {
    console.log(e.target.value);
})

themeMode.forEach((item) => {
    item.addEventListener('click', (e) => {
        document.body.classList.remove('darkTheme', 'salmonTheme')
        switch(e.target.id)
        {
            case "dark":
                document.body.classList.add('darkTheme');
                break;
            
            case "salmon":
                document.body.classList.add('salmonTheme');
                break;

            default:
                null;
        }
    })
})